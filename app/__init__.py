import dash
import dash_auth
import dash_html_components as html
import dash_core_components as dcc
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import urllib
import pandas as pd
import numpy as np
import os
from app.helpers.dataset import get_results
from app.helpers.figures import get_figure, get_figure_general_scores, get_data, get_chart_data, get_score_data

APP_NAME = os.getenv("APP_NAME","Sample Dashboard")
APP_ACCOUNT_NAME = os.getenv("APP_ACCOUNT_NAME","hello")
APP_ACCOUNT_PW = os.getenv("APP_ACCOUNT_PW","world")
VALID_USERNAME_PASSWORD_PAIRS = [[APP_ACCOUNT_NAME, APP_ACCOUNT_PW]]

# if there's no data set: create one.
try:
    df_results = pd.read_pickle('df_results.pkl')
except OSError:
    df_results = get_results()

headers = df_results.header.unique()
fysios = np.sort(df_results.fysiotherapeut.unique())
years = np.sort(df_results.year.unique())[::-1]
most_recent_year = years[0]

app = dash.Dash('auth')
app.config['suppress_callback_exceptions']=True

auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)
server = app.server

hide_mode_bar = {'displayModeBar': False}

app.layout = html.Div([
    # row 1
    html.Div([html.Div([
        html.H1('Dashboard CQI'),
        html.H3(APP_NAME),
    ], className='eight columns',
    ),
              html.Div([
                  dcc.Markdown(
                      id="summary",
                  )
              ], className="four columns",
                       style={'textAlign': 'right'}),
    ], className='row',
    ),
    # row 2
    html.Div([
        html.Div([
            html.Label("Kies fysiotherapeut (leeg is hele praktijk):"),
            dcc.Dropdown(
                id='dropdown_fysio',
                options=[{'label': i, 'value': i} for i in fysios],
                value=None
            ),
        ], className = 'four columns'
        ),
        html.Div([
            html.Label("Kies jaar:"),
            dcc.Dropdown(
                id='dropdown_year',
                options=[{'label': i, 'value': i} for i in years],
                value=most_recent_year
            ),
        ], className = 'three columns'
        ),
        html.Div([
            html.Label("Leeftijd:"),
            dcc.Dropdown(
                id='dropdown_age',
                options=[{'label': 'Iedereen', 'value': 'all'},
                         {'label': 'Kinderen', 'value': 'children'},
                         {'label': 'Volwassenen', 'value': 'adults'},
                ],
                value='all'
            )
        ], className = 'three columns'
        ),
    ], className = 'row',
    ),
    # row 3
    html.Div([
        html.A(
            'download data',
            id='download-link',
            download="data-survey.csv",
            href="",
            target="_blank",
            className = 'four columns',
        ),
        html.A(
            'meer informatie over DASH',
            id='dash-link',
            href="https://plot.ly/products/dash/",
            target="_blank",
            className = 'four columns',style={'textAlign': 'center'},
        ),
        html.A(
            'source code',
            id='source-code-link',
            href="https://bitbucket.org/saggio/mq-mh-dash",
            target="_blank",
            className = 'four columns',style={'textAlign': 'right'},
        )], className = 'row',
    ),
    # row 4
html.Div([
    html.Div([
        dcc.Graph(id='cijfer',
                  config=hide_mode_bar),
    ], className='six columns',
    ),
    html.Div([
        dcc.Graph(id='aanbeveling',
                  config=hide_mode_bar),
    ], className='six columns',
    )
], className='row'
),
    # row 5
    html.Div([
        dcc.Graph(id='behandelplan',
                  config=hide_mode_bar),
    ], className='row'),
    # row 6
    html.Div([
        dcc.Graph(id='praktijk',
                  config=hide_mode_bar),
    ], className='row',
    ),
    # row 7
    html.Div([
        dcc.Graph(id='contact',
                  config=hide_mode_bar),
    ], className='row',
    ),
], className='container',
                      style={
                          'width': '85%',
                          'max-width': '1200',
                          'margin-left': 'auto',
                          'margin-right': 'auto',
                          'padding-top': '20',
                          'padding-bottom': '20',})


def update_graph(fysio, year, age, header):
    df_, n, mean, mean_last_year = get_chart_data(df_results,
                                                  header=header,
                                                  fysio=fysio,
                                                  year=year,
                                                  age=age)
    annotations = ["gem {0:.2f}({1:.2f}) N={2}".format(m, mly, n)
                   for m, mly, n  in zip(mean,mean_last_year, n)]

    data, layout = get_figure(df_,title=header,annotations=annotations)

    return {
        'layout': layout ,
        'data': data
    }

def update_general_scores_graph(fysio, year, age, question):
    s_, title = get_score_data(df_results, fysio=fysio, year=year,age=age,q=question)
    s_ += 1

    data, layout = get_figure_general_scores(s_, title=title)

    return {
        'layout': layout ,
        'data': data
    }

inputs = [dash.dependencies.Input('dropdown_fysio', 'value'),
          dash.dependencies.Input('dropdown_year', 'value'),
          dash.dependencies.Input('dropdown_age', 'value')]

@app.callback(
    dash.dependencies.Output('contact', 'figure'),
    inputs)
def update(fysio, year, age):
    return update_graph(fysio, year, age,'CONTACT MET DE FYSIOTHERAPEUT')

@app.callback(
    dash.dependencies.Output('behandelplan', 'figure'),
    inputs)
def update(fysio, year, age):
    return update_graph(fysio, year, age, 'BEHANDELPLAN')

@app.callback(
    dash.dependencies.Output('praktijk', 'figure'),
    inputs)
def update(fysio, year, age):
    return update_graph(fysio, year, age, 'DE PRAKTIJK')

@app.callback(
    dash.dependencies.Output('cijfer', 'figure'),
    inputs)
def update(fysio, year, age):
    return update_general_scores_graph(fysio, year, age, 0)

@app.callback(
    dash.dependencies.Output('aanbeveling', 'figure'),
    inputs)
def update(fysio, year, age):
    return update_general_scores_graph(fysio, year, age, 1)

@app.callback(
    dash.dependencies.Output('summary', 'children'),
    inputs)
def update(fysio, year, age):
    score, score_last_year = get_score_data(df_results,
                                            fysio=fysio,
                                            year=year,
                                            age=age,
                                            q=0)

    m = score.mean()[0] + 1
    text = '''gemiddelde tevredenheid
# {:.1f}'''.format(m)
    return text

@app.callback(
    dash.dependencies.Output('download-link', 'href'),
    inputs)
def update_download_link(fysio, year, age):
    df_, _ = get_data(df_results, fysio=fysio,year=year, age=age)

    csv_string = df_.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)

    return csv_string

app.scripts.config.serve_locally = True
app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})

if __name__ == '__main__':
    app.run_server(debug=True)
