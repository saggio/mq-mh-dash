import pandas as pd
import numpy as np
import names

from app.helpers.answers import get_answers
# get questions
df_questions = pd.read_csv("app/helpers/vragenlijst.csv")

# and answers
answers = get_answers()

# split question in number and description
df_ = df_questions.question.str.extract(r"(^\d+)\.? ?(.*)", expand=True)
df_questions = pd.concat([df_questions, df_], axis=1)
df_questions = df_questions.drop(["question"], axis=1)
df_questions = df_questions.rename(columns={0: "nr", 1: "question"})
df_questions.nr = df_questions.nr.apply(int)


# expand answers to full list with answers
df_questions['list_with_answers'] = df_questions.answer.apply(lambda x: answers.get(x))

# these items are not included yet
#df_questions = df_questions.append({'header': 'OVER UZELF', 'list_with_answers': ['male','female'],'nr': 98, 'question': 'Geslacht'}, ignore_index=True)
#df_questions = df_questions.append({'header': 'OVER UZELF', 'list_with_answers': range(100),'nr': 99, 'question': 'Leeftijd'}, ignore_index=True)
#

# we will have 4 physiotherapist with random names
fysios = [name() for name in [names.get_full_name] * 4]

# not all physiotherapist are equally good, so we'll give some
# therapists a (dis)advantage
handicap_fysios = {f: h for f, h in zip(fysios, [-1,0,1,0])}

df_results = pd.DataFrame()

survey_id = 1
patient_id = 1

def get_age():
    age = np.random.normal(30,30,1)
    if age < 0 or age > 100:
        age = np.random.choice(range(18))
    return int(age)

# we'll add the following number of lists for each year
for y, n in zip([2016,2017,2018],[200,210,208]):
    for i in range(n):
        df_questions['survey_id'] = survey_id
        df_questions['year'] = y

        # get random answers from the list
        df_questions['result'] = df_questions.list_with_answers.apply(lambda x: np.random.randint(len(x)))

        # choose therapist (litte bit biased)
        fysio = np.random.choice(fysios,p=[0.2,0.2,0.3,0.3])
        df_questions['fysiotherapeut'] = fysio

        # add a little bit variation per therapist
        handicap = handicap_fysios[fysio]
        chance = np.random.choice([True,False], 1, p=[0.45,0.55])[0]
        df_questions['result'] = df_questions.apply(lambda x: x.result + handicap if chance
                            and (handicap + x.result) > 0
                            and (handicap + x.result) < len(x.list_with_answers)
                            else x.result, axis=1)

        gender = np.random.choice(['male', 'female'])
        df_questions['gender'] = gender
        df_questions['age'] = get_age()
        name_patient = names.get_full_name(gender=gender)
        df_questions['patient'] = name_patient

        # check whether name is already known
        if 'patient' in df_results.columns and name_patient in df_results.patient.unique():
            # if so: use existing patient_id
            patient_id = df_results[df_results.patient == name_patient].iloc[0].patient_id
            # and also existing age
            age = df_results[df_results.patient == name_patient].iloc[0].age

            df_questions['age'] = age

            df_questions.loc[df_questions.question == 'Leeftijd', 'result'] = age

            print(name_patient)
        else:
            patient_id += 1

        df_questions['patient_id'] = patient_id

        df_questions["id"] = survey_id
        # add this filled in list to the dataframe with all the results
        df_results = df_results.append(df_questions)

        survey_id += 1

# these are the columns we need
df_results = df_results[['header','nr','survey_id','year','question','result','answer','fysiotherapeut','patient_id','patient','age','gender']]

#df_results.to_pickle('df_results.pkl')

def get_results():

    return df_results
