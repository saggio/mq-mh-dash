# Het werkt maar deze code kan stuk efficienter...

import pandas as pd
import numpy as np
import plotly.graph_objs as go


eensoneens = """Helemaal oneens
    Oneens
    Niet oneens, niet eens
    Eens
    Helemaal eens
    Weet ik niet/n.v.t"""

def split_line(line, pos=40):
    if len(line) <= pos:
        return line
    last_space = line[:pos][::-1].find(" ")
    pos -= last_space

    return line[:pos-1] + "<br>" + line[pos:]

def get_data(df, header=None, fysio=None, year=None, age='all'):

    if header != None:
        df = df[(df.header == header)]

    if fysio != None:
        df = df[(df.fysiotherapeut == fysio)]

    if age == 'children':
        df = df[df.age <= 18]

    if age == 'adults':
        df = df[df.age > 18]

    df_last_year = pd.DataFrame()
    if year != None:
        df_last_year = df[df.year == year - 1].copy()
        df = df[df.year == year]

    return df, df_last_year

def get_score_data(df, fysio=None, year=None, age='all', q=0):
    df, _ = get_data(df, header='ALGEMENE BEOORDELING', fysio=fysio, year=year, age=age)
    question = df.question.unique()[q]

    s = df.loc[df.question == question,['fysiotherapeut','result']].set_index('fysiotherapeut')

    return s, question

def get_chart_data(df, header=None, fysio=None, year=None, age='all'):

    df, df_last_year = get_data(df, header=header, fysio=fysio, year=year, age=age)
    # fill in 0 if there is no last year
    mean_last_year = [0] * len(df.question.unique())
    if len(df_last_year) > 0:
        mean_last_year = df_last_year.groupby(['question'])['result'].mean().values[::-1]

    mean = df.groupby(['question'])['result'].mean().values[::-1]

    df = df.groupby(['question','result']).size().unstack()

    df.columns = eensoneens.split("\n")
    df = df.iloc[:,:-1]
    n = df.sum(axis=1).values[::-1]

    df = df.T.div(df.sum(axis=1)).T
    df = round(df * 100,1).reset_index()
    df['question'] = df['question'].apply(split_line)

    return df, n, mean, mean_last_year

def get_figure_data(colors, df):
    data = []
    for c, i in colors:
        data.append(go.Bar(
            y=df['question'],
            x=df.iloc[:,i],
            name=df.columns[i],
            marker=dict(
                color=c)
            ,orientation = 'h', opacity=0.6),)
    return data


def get_figure(df, annotations=[], title=''):

    colors = list(zip(['darkred','red','grey','green','darkgreen'],np.arange(5)+1))
    data = get_figure_data(colors, df)

    if len(annotations) > 0:
        annotations=[
            dict(
                x=110,
                y=i,
                xref='x',
                yref='y',
                text=n,
                showarrow=False,
            ) for i, n in enumerate(annotations)]

    layout = go.Layout(
        barmode='stack',
        title=title,
        height=70+100+150,
        showlegend=True,
        legend=dict(orientation="h", traceorder="normal"),
        margin=go.Margin(
            l=250,
            r=0,
            b=100,
            t=70,
            #pad=30
        ),
        xaxis=go.XAxis(
            zeroline=False),
        annotations=annotations
    )

    return data, layout

def get_figure_general_scores(df, title='pppp'):
    title = split_line(title,50)
    traces = []
    for fysio in df.index.unique():
        traces.append(go.Box(
            y=df.loc[fysio,'result'].values,
            name=fysio,
            boxpoints='all',
            jitter=0.5,
            whiskerwidth=0.2,
            marker=dict(
                size=4,
            ),
            line=dict(width=1),
            boxmean='sd',
        ))

    layout = go.Layout(
        autosize=True,
        boxgap=0.33,
        boxgroupgap= 0.29,
        height= 400,
        margin= go.Margin(
            r= 30,
            t= 100,
            b= 80,
            l= 40),
        showlegend=False,
        title= title,
        width= 600,
        yaxis= go.YAxis(
            autorange= True,
            dtick= 1,
            gridwidth= 1,
            showgrid= True,
            tickmode= 'linear',
            type= 'linear',
            zeroline= False,
            zerolinewidth= 1
        )
    )

    return traces, layout
