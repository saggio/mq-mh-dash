answers = {}

eensoneens = """Helemaal oneens
Oneens
Niet oneens, niet eens
Eens
Helemaal eens
Weet ik niet/n.v.t"""
answers["eensoneens"] = eensoneens.split("\n")

verbeterd = """Heel veel verbeterd
Veel verbeterd
Iets verbeterd
Hetzelfde
Iets verslechterd
Veel verslechterd
Heel veel verslechterd
Weet ik niet/n.v.t"""
answers["verbeterd"] = verbeterd.split("\n")

answers["nultien"] = range(1,11)

opleiding = """Geen opleiding (lager onderwijs: niet afgemaakt)
Lager onderwijs (basisschool, speciaal basisonderwijs)
Lager of voorbereidend beroepsonderwijs (zoals LTS, LEAO, LHNO, VMBO)
Middelbaar algemeen voortgezet onderwijs (zoals MAVO, (M)ULO, MBO-kort, VMBO-t)
Middelbaar beroepsonderwijs en beroepsbegeleidend onderwijs (zoals
MBO-lang, MTS, MEAO, BOL, BBL, INAS)
Hoger algemeen en voorbereidend wetenschappelijk onderwijs (zoals HAVO, VWO, Atheneum, Gymnasium, HBS, MMS)
Hoger beroepsonderwijs (zoals HBO, HTS, HEAO, HBO-V, kandidaats wetenschappelijk onderwijs)
Wetenschappelijk onderwijs (universiteit)
Anders"""
answers["opleiding"] = opleiding.split("\n")

uitstekendslecht = """Uitstekend
Zeer goed
Goed
Matig
Slecht"""
answers["uitstekendslecht"] = uitstekendslecht.split("\n")

def get_answers():
    return answers
